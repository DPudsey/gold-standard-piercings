import type { Metadata } from "next";
import { GoogleTagManager } from "@next/third-parties/google";
import { Inter } from "next/font/google";
import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Gold Standard Piercings",
  description:
    "Gold Standard Piercings & Fine Jewellery Beauty, cosmetic & personal care. Titanium & Solid Gold Safe, Sterile & Disposable Equipment by @louisexvxpiercer & @tom.needles",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <GoogleTagManager gtmId="GTM-WG8XBQ8K" />
      <body className={inter.className}>{children}</body>
    </html>
  );
}
