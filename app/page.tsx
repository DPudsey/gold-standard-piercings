import Image from "next/image";
import typography from "./scss/typography.module.scss";
import furniture from "./scss/furniture.module.scss";

export default function Home() {
  return (
    <main className="flex justify-center min-h-screen p-2.5 antialiased">
      <div className="flex flex-col gap-y-2.5 max-w-screen-sm">
        <div className="relative flex place-items-center py-2.5">
          <Image
            className=""
            src="/gold-standard-logo.png"
            alt="Gold Standard Logo"
            width={200}
            height={103}
            priority
          />
        </div>

        <div className="relative flex flex-col">
          <header className="py-2.5">
            <h1>
              Gold Standard Piercings & Fine Jewellery is Camden Town&apos;s
              first fully disposable and dedicated Piercing only studio. We
              pride ourselves on our beautiful selection of high quality
              jewellery, service and hygiene.
            </h1>
          </header>
          <div className={furniture.WhatsAppWrapper}>
            <a
              className={furniture.WhatsAppLink}
              href="https://wa.me/+447932374664"
            >
              Chat on WhatsApp
            </a>
          </div>
          <section className="py-2.5">
            <h2 className={typography.Heading}>Opening times</h2>
            <div className={typography.TimeSlot}>
              <p className={typography.NoMargin}>
                <strong>Monday to Saturday</strong>
              </p>
              <p className={typography.NoMargin}>
                11.00am - 7pm. Last service 6.30pm
              </p>
            </div>
            <div className={typography.TimeSlot}>
              <p className={typography.NoMargin}>
                <strong>Sunday</strong>
              </p>
              <p className={typography.NoMargin}>
                11.00am - 5pm. Last service 4.30pm
              </p>
            </div>
          </section>
          <section className="py-2.5">
            <h2 className={typography.Heading}>Book</h2>
            <div className="flex flex-col gap-y-2.5">
              <p className={typography.NoMargin}>
                <a
                  className={typography.ContactLink}
                  href="https://www.instagram.com/goldstandardpiercings"
                  target="_blank"
                >
                  <Image
                    className={furniture.ContactIcon}
                    src="/instagram.svg"
                    alt="Instagram"
                    width={200}
                    height={103}
                  />
                  @goldstandardpiercings
                </a>
              </p>
              <p className={typography.NoMargin}>
                <a
                  className={typography.ContactLink}
                  href="mailto:goldstandardpiercings@gmail.com"
                >
                  <Image
                    className={furniture.ContactIcon}
                    src="/email.svg"
                    alt="Email"
                    width={200}
                    height={103}
                  />
                  goldstandardpiercings@gmail.com
                </a>
              </p>
              <p className={typography.NoMargin}>
                <a
                  className={typography.ContactLink}
                  href="tel:+44 79323 74664"
                >
                  <Image
                    className={furniture.ContactIcon}
                    src="/telephone.svg"
                    alt="Telephone"
                    width={200}
                    height={103}
                  />
                  +44 79323 74664
                </a>
              </p>
            </div>
          </section>
          <section className="py-2.5">
            <h2 className={typography.Heading}>Address</h2>
            <p className={typography.NoMargin}>
              <a
                className={typography.ContactLink}
                href="https://maps.app.goo.gl/QU3Ly1hNbGm3aF388"
                target="_blank"
              >
                <Image
                  className={furniture.ContactIcon}
                  src="/address.svg"
                  alt="Address"
                  width={200}
                  height={103}
                />
                Unit 8, 190 Camden High Street, NW1 8QP
              </a>
            </p>
          </section>
          <section className="py-2.5">
            <h2 className={typography.Heading}>Price list</h2>
            <div className={furniture.PriceListRow}>
              <div className={furniture.PriceListColumn}>
                <p className={typography.PriceItemNoMargin}>
                  Check up fee<strong>£5</strong>
                </p>
                <p className={typography.PriceItem}>
                  <i>For piercings done elsewhere</i>
                </p>
                <p className={typography.PriceItem}>
                  Jewellery Fitting / Removal <strong>£10</strong>
                </p>
                <p className={typography.PriceItem}>
                  Microdermal Removal <strong>£15</strong>
                </p>
                <p className={typography.PriceItem}>
                  Taper open / reopen without a needle <strong>£15</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Lobe <strong>£30</strong>
                </p>
                <p className={typography.PriceItem}>
                  - Pair / stack <strong>£55</strong>
                </p>
                <p className={typography.PriceItem}>
                  Cartilage / flat / tragus / helix / conch <strong>£40</strong>
                </p>
                <p className={typography.PriceItem}>
                  Snug <strong>£40</strong>
                </p>
                <p className={typography.PriceItem}>
                  Daith <strong>£40</strong>
                </p>
                <p className={typography.PriceItem}>
                  Rook <strong>£40</strong>
                </p>
                <p className={typography.PriceItem}>
                  Industrial <strong>£50</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Nose <strong>£40</strong>
                </p>
                <p className={typography.PriceItem}>
                  - Pair <strong>£75</strong>
                </p>
              </div>
              <div className={furniture.PriceListColumn}>
                <p className={typography.PriceItemNoMargin}>
                  High nostril <strong>£45</strong>
                </p>
                <p className={typography.PriceItem}>
                  - Pair <strong>£85</strong>
                </p>
                <p className={typography.PriceItem}>
                  Septum <strong>£50</strong>
                </p>
                <p className={typography.PriceItem}>
                  Eyebrow <strong>£40</strong>
                </p>
                <p className={typography.PriceItem}>
                  Bridge <strong>£45</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Lip (labret style jewellery) <strong>£45</strong>
                </p>
                <p className={typography.PriceItem}>
                  - Pair <strong>£85</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Lip (curved bar)
                  <strong>£55</strong>
                </p>
                <p className={typography.PriceItem}>
                  - Pair <strong>£105</strong>
                </p>
                <p className={typography.PriceItem}>
                  Cheeks / dahlias <strong>£105</strong>
                </p>
                <p className={typography.PriceItem}>
                  Tongue <strong>£50</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Nipple <strong>£45</strong>
                </p>
                <p className={typography.PriceItem}>
                  - Pair <strong>£85</strong>
                </p>
                <p className={typography.PriceItem}>
                  Navel <strong>£45</strong>
                </p>
              </div>
            </div>
          </section>
          <section className="py-2.5">
            <h2 className={typography.Heading}>About</h2>
            <p>
              Each piercing is performed with single use sterile needles and any
              equipment used is immediately disposed of.
            </p>
            <p>
              The jewellery we offer is of the highest industry standard. We
              only use Internally Threaded or Threadless Titanium, both of which
              have a mirror polish finish, assuring comfort and safety during
              insertion and wear. Our Gold jewellery is solid 14k & 18k with no
              platings, fillers or coatings.
            </p>
            <p>
              We are the only studio in Camden Town to offer lobe piercings done
              with a needle for children from 8 & up (by appointment only) using
              high quality jewellery. This is the safest way for children (and
              adults, of course!) to be pierced. (A parent or legal guardian
              MUST be present with photo ID for both adult and child for the
              piercings procedure)
            </p>
          </section>
          <section className="py-2.5">
            <h2 className={typography.Heading}>
              How much does a piercing cost?
            </h2>
            <p>
              Our piercing price list covers the cost of a piercing with basic
              titanium jewellery.
            </p>
            <p>
              But what if you want something more fancy or sparkly in your new
              piercing? We have a huge range of upgrade options available and of
              course the cost of each piece will vary. Our inventory also
              changes a lot but here we will outline some of the options we have
              available for piercings and jewellery changes. Items listed here
              are verified Implant Grade Titanium unless otherwise stated. All
              CZ (Cubic Zirconia) stones are genuine and come in a variety of
              colour options. Opals are genuine and synthetic depending on
              manufacturer, available in a variety of colours.
            </p>
            <div className={furniture.PriceListRow}>
              <div className={furniture.PriceListColumn}>
                <p className={typography.PriceItem}>
                  <strong>Upgrade Attachment (Top Only)</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  1.5mm - 2.5mm CZ/Opal <strong>£25</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  3mm - 4mm CZ/Opal <strong>£30</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  5mm CZ/Opal <strong>£35</strong>
                </p>
                <p className={typography.PriceItem}>
                  6mm CZ/Opal <strong>£40</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Large Forte/Flower CZ <strong>£40</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Small Forte/Flower CZ <strong>£35</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Large Trinity CZ <strong>£35</strong>
                </p>
                <p className={typography.PriceItem}>
                  Small Trinity CZ <strong>£30</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Trillion CZ <strong>£30</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  3mm Pear CZ <strong>£30</strong>
                </p>
                <p className={typography.PriceItem}>
                  4mm Pear CZ <strong>£35</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Opal Marquise <strong>£35</strong>
                </p>
                <p className={typography.PriceItem}>
                  Canasteel Navel Bar (no top) <strong>£35</strong>
                </p>
                <p className={typography.PriceItem}>
                  Titanium shapes <strong>£15</strong>
                </p>
              </div>
              <div className={furniture.PriceListColumn}>
                <p className={typography.PriceItem}>
                  <strong>Titanium Basics (Jewellery only)</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Ball <strong>£5</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Labret stem <strong>£10</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Barbell Stem <strong>£10</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Threadless Curved Barbell Stem <strong>£10</strong>
                </p>
                <p className={typography.PriceItem}>
                  Internally Threaded Barbell Stem <strong>£15</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Labret w/ball <strong>£15</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Barbell
                  <strong>£20</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Threadless Curved Barbell <strong>£20</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Internally Threaded Curved Barbell <strong>£25</strong>
                </p>
                <p className={typography.PriceItem}>
                  Circular Barbell/Horseshoe <strong>£25</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Niobium Captive Bead Ring <strong>£20</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Niobium Plain Seam Ring <strong>£15</strong>
                </p>
                <p className={typography.PriceItemNoMargin}>
                  Apex Niobium Textured Seam Ring <strong>£20</strong>
                </p>
                <p className={typography.PriceItem}>
                  Apex Niobium Teardrop <strong>£25</strong>
                </p>
                <p className={typography.PriceItem}>
                  Anatometal Navel curves <strong>£55</strong>
                </p>
              </div>
            </div>
            <p>
              <strong>
                We also offer a wide range of Solid Gold Options in both 14
                carat and 18 carat (Yellow, White & Rose Gold). The price of
                Gold and our inventory is more variable but our upgrade options
                in Solid Gold generally begin at around £50+ (attachment only).
              </strong>
            </p>
            <p>
              So for example, a nostril piercing (£40) with a 2.5mm Cubic
              Zirconia (CZ) attachment (£25) will cost £65. Or, a pair of ear
              lobe piercings (£55) with two small Trinity CZs (£30 each) will
              cost £115.
            </p>
            <p>
              There are many different factors which can determine the price of
              a piercing and depending on a potential customer&apos;s
              perspective it may seem expensive. Many people will attempt to
              seek out the cheapest possible option to fulfil their desire to
              get a piercing and at face value this may seem like the most cost
              effective means of getting what they want. As we all know going
              for the cheapest possible option does not generally lend to the
              best experience or outcome. But why is there such a difference in
              prices for piercing services? We will attempt to explain this
              below for those who are interested.
            </p>
            <p>
              There are three key elements which will vary from studio to studio
              to take into consideration, labour, setup and jewellery.
              Ultimately everything we do as a studio is a reflection of our
              business and us as professionals, the standards we have in place
              are based on years of experience and forever trying to improve our
              knowledge. We use the best possible quality products and try to
              deliver them at the most accessible price. We of course accept
              that not everyone may have the budget for the services we offer
              but a piercing is an investment and can last a lifetime so we
              believe it is better to spend a bit more initially rather than
              compromising on quality and enjoyment.
            </p>
            <p>
              As a fully disposable studio, we do not reprocess anything that we
              use, this means we can operate without needing facilities to
              reprocess contaminated tools etc while also significantly reducing
              the risk of cross contamination, ultimately it is more hygienic
              and safe. Everything we use during the piercing process costs
              money and this needs to be taken into consideration when setting
              our price point, the fact we use sterile gloves for piercing
              procedures is also a significant financial outlay.
            </p>
            <p>
              All of the jewellery we use in our studio is verified by the
              Association of Professional Piercers, this means that the
              manufacturers are held accountable for the quality and grade of
              the materials which they use and distribute. Our basic jewellery
              is a combination of Junipurr Jewelry and NeoMetal and we use
              either threadless or internally threaded systems as standard.
              These companies provide mill certificates for the Titanium they
              use which proves the source and Implant Grade status of their
              products. When we opened Gold Standard Piercings and Fine
              Jewellery we made a conscious decision that we would only use the
              highest quality body jewellery for our clients. Better quality
              services and materials makes for healthier piercings and more
              comfortable clients. Consumers invest a huge amount of trust in us
              as professionals and we feel that it is only right that we share
              some insight into our pricing and costs. Hopefully this offers
              some explanation as to why our services cost what they do, if you
              have any further questions please feel free to contact us.
            </p>
          </section>
          <section className="py-2.5">
            <h2 className={typography.Heading}>Piercing / ID Policy</h2>
            <ul>
              <li>
                No person under the age of 16 shall be pierced except on the ear
                lobe with the consent of a parent or legal guardian who must be
                present and sign the consent form, valid photo ID is also
                required for both consenting adult and minor.
              </li>
              <li>Ear lobe - ages 8+ (with parental consent)</li>
              <li>
                Nipple, Cheek, Bridge and Male Genital piercing - ages 18+
              </li>
              <li>
                Female genital piercing is forbidden by law in the borough of
                Camden.
              </li>
              <li>We do not currently offer Microdermal piercings.</li>
              <li>
                Valid photo ID is a mandatory requirement for the consent form.
              </li>
              <li>
                Pricing includes basic Titanium jewellery, any gem/attachment
                used for piercing will incur additional cost.
              </li>
              <li>
                We will not pierce with or fit any jewellery which has not been
                purchased from Gold Standard Piercings & Fine Jewellery.
              </li>
              <li>
                Due to the fully disposable nature of the studio we cannot
                pierce with jewellery previously purchased from us also.
              </li>
              <li>All piercings are anatomy dependant.</li>
            </ul>
          </section>
          <section className="py-2.5 pb-20">
            <h2 className={typography.Heading}>Meet the team</h2>
            <div className={furniture.TeamMembers}>
              <div className={furniture.TeamMember}>
                <p>
                  <strong>Louise</strong>
                </p>
                <p>
                  <a
                    className={typography.ContactLink}
                    href="https://www.instagram.com/louisexvxpiercer/"
                    target="_blank"
                  >
                    <Image
                      className={furniture.ContactIcon}
                      src="/instagram.svg"
                      alt="Instagram"
                      width={200}
                      height={103}
                    />
                    <strong>@louisexvxpiercer</strong>
                  </a>
                </p>
                <Image
                  className={furniture.ProfilePicture}
                  src="/Louise-profile-picture.jpg"
                  alt="Louise profile picture"
                  loading="lazy"
                  width={373}
                  height={663}
                />
                <p>
                  Growing up as a creative and artsy kid, Louise found herself
                  never sure of what she wanted to do for a career. The idea of
                  doing the same job for years and years seemed boring,
                  especially after working office jobs in between completing her
                  Degrees.After moving to London to study Fashion Design (which
                  she enjoyed but again, wasn&apos;t what she really wanted)
                  Louse eventually found herself in a position to learn to be a
                  piercer.
                </p>
                <p>
                  When she began in 2010, piercing was still pretty old school.
                  Not a lot of choices for jewellery, but she felt like
                  she&apos;d found her place in life. There was just something
                  so exciting about it all, every day was different.
                </p>
                <p>
                  And her she is many years later, not at all bored at doing the
                  same job. Louise&apos;s enthusiasm and passion for piercing is
                  seen in her work and they way she speaks to clients
                  individually to plan their piercings.Clients are drawn to her
                  excitement and creativity, her use of colour and texture along
                  with placement.As someone with a passion for jewellery and
                  especially gold, there isn&apos;t a better career.
                </p>
                <p>
                  When Louise isn&apos;t piercing she can be found outdoors with
                  her family, feeding squirrels or lifting heavy things and
                  always in Nike.
                </p>
              </div>
              <div className={furniture.TeamMember}>
                <p>
                  <strong>Tom</strong>
                </p>
                <p>
                  <a
                    className={typography.ContactLink}
                    href="https://www.instagram.com/tom.needles/"
                    target="_blank"
                  >
                    <Image
                      className={furniture.ContactIcon}
                      src="/instagram.svg"
                      alt="Instagram"
                      width={200}
                      height={103}
                    />
                    <strong>@tom.needles</strong>
                  </a>
                </p>
                <Image
                  className={furniture.ProfilePicture}
                  src="/Tom-profile-picture.jpg"
                  alt="Tom profile picture"
                  loading="lazy"
                  width={373}
                  height={497}
                />
                <p>
                  Tom has been piercing since the beginning of 2016 and is also
                  a veteran of the service industry, he has forged a strong
                  reputation based on his aim to consistently achieve the best
                  overall client experience possible, alongside his skill set
                  and attentive bedside manner. Tom feels it is important for
                  people to be as informed as possible with regard to body
                  piercings so will try and convey as much useful information to
                  help ensure clients are fully equipped for the commitment of
                  healing a piercing.
                </p>
                <p>
                  While Tom does not favour particular areas of the body to
                  pierce he does enjoy the challenge that is presented with each
                  individual&apos;s anatomy and the opportunity to create unique
                  styles and placements. His favourite ear piercing to perform
                  is a conch on the basis that it was his first ear piercing but
                  also the versatility the placement offers in terms of
                  jewellery options.
                </p>
                <p>
                  Outside of piercing Tom enjoys cycling, music, cooking,
                  reading and socialising.
                </p>
              </div>
            </div>
          </section>
        </div>
      </div>
    </main>
  );
}
